Datum: 19.03.2020
Start: 20:00 Uhr
Ende : 20:.. Uhr

Ort: DIGITAL CHURCH, Jülicher Str. 72a, 52070 Aachen

Protokoll: Jan Niehusmann

---

__Anwesend__
- ... Mitglieder persönlich, ... Mitglieder in Vertretung
  - siehe Anwesenheitsliste

---

# [TOP 1] Begrüßung
Sarah begrüßt alle Anwesenden und heißt sie zu der
Mitgliederversammlung 2020 des Fördervereins für freie Netzwerke
e.V. herzlich willkommen.

Aufgrund der Corona-Pandemie wird die Mitgliederversammlung im minimal
möglichen Rahmen durchgeführt.

# [TOP 2] Formalia

Die Einladung erfolgte frist- und formgerecht.

Die Mitgliederversammlung ist laut Satzung ohne Rücksicht auf die Zahl der
erschienenen Mitglieder beschlussfähig.

Die Anwesenden wählen Sarah Beschorner einstimmig zur/zum Sitzungsleiter/in.
Die Anwesenden wählen Jan Niehusmann einstimmig zur/zum Protkollführer/in.

# [TOP 3] Bericht des Vorstands

Der Jahresbericht 2019 des Vereins wurde im Vorfeld der Mitgliederversammlung per
Mail an alle Mitglieder verteilt. Die Versammlung nimmt zur Kenntnis, dass folgende
Anmerkungen von Mitgliedern per Mail zu Protokoll gegeben wurden:

...

# [TOP 4] Bericht der Kassenprüfer

Der Kassenbericht 2019 wurde den Mitgliedern zusammen mit der Einladung
zur Mitgliederversammlung zugeschickt. Zuvor wurde der Kassenbericht und die Kasse
von Kassenprüfer Daniel Müllers geprüft.

Es wurden keine Beanstandungen festgestellt.

Die beiden anderen Kassenprüfer waren am Prüfungstermin verhindert,
konnten die Unterlagen aber auf digitalem Weg einsehen und hatten keine
Einwände.

# [TOP 5] Entlastung des Vorstands

Auf Grund des Kassens- und Jahresberichts wird mit folgendem Ergebnis über die
Entlastung des Vorstands abgestimmt.

- Dafür:         ...
- Dagegen:       ...
- Enthaltungen:  ...

# [TOP 6] Wahl des Vorstands

Der amtierende Vorstand steht vollständig erneut zur Wahl. Weitere Kandidaten
gibt es nicht.

Es wird über jeden Kandidaten einzeln abgestimmt mit dem folgenden
Ergebnis:

- Wahl - 1. Vorsitz- Kandidatin: Sarah Beschorner
  - Dafür:         ...
  - Dagegen:       ...
  - Enthaltungen:  ...

- Wahl - 2. Vorsitz - Kandidat: Dennis Crakau
  - Dafür:         ...
  - Dagegen:       ...
  - Enthaltungen:  ...

- Wahl - Kassenwart - Kandidat: Jan Niehusmann
  - Dafür:         ...
  - Dagegen:       ...
  - Enthaltungen:  ...

- Wahl - Beisitzer - Kandidat: Malte Möller
  - Dafür:         ...
  - Dagegen:       ...
  - Enthaltungen:  ...

- Wahl - Beisitzer - Kandidat: Arne Bayer
  - Dafür:         ...
  - Dagegen:       ...
  - Enthaltungen: ...

- Wahl - Beisitzer - Kandidat: Matthias Schmidt
  - Dafür:         ...
  - Dagegen:       ...
  - Enthaltungen:  ...

Alle gewählten Kandidaten haben im Vorfeld erklärt, die Wahl anzunehmen.

Die bisherigen Kassenprüfer stehen auch für das laufende Jahr wieder zur Verfügung.
- Oliver Huberty
- Christop Franzen
- Daniel Müllers

Es wird über alle drei Kandidaten gemeinsam abgestimmt.

Ergebnis der Wahl zur Kassenprüfung:
- Dafür:         ...
- Dagegen:       ...
- Enthaltungen:  ...


# [TOP 7] Berufung von Teams

Um die Versammlung kurz zu halten, werden an den Teams keine
Veränderungen vorgenommen. Die bestehenden Teams sind:

- Team Düren
- Team Freifunktag
- Team Server/ Backbone
- Team Baesweiler (Beggendorf)
- Team Herzogenrath

Die Teams werden gebeten, einen kurzen Jahresbericht zu verfassen,
der den Mitgliedern zur Verfügung gestellt werden kann.

# [TOP 8] Geplante Aktivitäten

Es wird auf den Jahresbericht verwiesen.

# [TOP 9] Verschiedenes

Dem Vorstand ist bewusst, dass die durch die Corona-Pandemie stark verkürzte MV nicht den
üblichen Diskurs mit den Vereinsmitgliedern ermöglicht. Diese Form wurde lediglich gewählt
um Satzungsgemäß (§13.3) im 1. Quartal die MV abhalten zu können.
Sofern sich nach Abklingen der Corona-Pandemie der Bedarf für eine außerordentliche MV ergeben sollte,
wird der Vorstand auch bei Unterschreitung der in §13.5.1 der Satzung vorgesehen Quote zu einer außerordentlichen
MV einladen.

# [TOP 10] Abschluss der Versammlung

... bedankt sich für die Mitarbeit und verabschiedet sich bei allen
Anwesenden.

Ende der MV um 20:.. Uhr.

<br />
<br />
<br />
<br />
<br />
(... / Protokollführerin)

<br />
<br />
<br />
<br />
<br />
(... / Sitzungsleitung)

