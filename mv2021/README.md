Informationen zur Mitgliederversammlung am 29.4.2021
====================================================
(Stand 15.4.2021)

Liebe Mitglieder,

zur Videokonferenz der Mitgliederversammlung geht es hier entlang: [Videkonferenz](https://meet.f3n-ac.de/F3N-MV-2021)

Der Jahresbericht kann hier eingesehen werden: [Jahresbericht](https://gitlab.com/f3n/info/-/blob/master/mv2021/jahresbericht2021.txt)

Den Kassenbericht findet Ihr hier: [Kassenbericht](https://gitlab.com/f3n/info/-/blob/master/mv2021/Kassen_und_Prüfbericht_2020.txt)

Die Kasse wurde von Christoph Franzen, Daniel Müllers und Felix
Preuschoff geprüft. Unterschriften werden wir nachreichen, da die
Prüfung wegen Corona vollständig online erfolgte.

Bitte stellt nach Möglichkeit offene Fragen bereits im Vorfeld, damit
nicht wärend der Versammlung die erfragten Informationen zusammensuchen
müssen.


Euer F3N-Vorstand

