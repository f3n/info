Informationen zur Mitgliederversammlung am 10.04.2024, 19 Uhr
=============================================================
(Stand 29.03.2024)

Liebe Mitglieder,

die Versammlung findet hier statt: [RelAix Networks GmbH, Auf der Hüls 172, 52068 Aachen](https://www.openstreetmap.org/search?query=Auf%20der%20H%C3%BCls%20172%2C%2052068%20Aachen#map=19/50.78353/6.13472)  

Den Kassenbericht findet Ihr hier: [Kassenbericht](https://gitlab.com/f3n/info/-/blob/master/mv2024/Pr%C3%BCfbericht2023-signed.pdf)

Ergänzung der Tagesordnung
--------------------------

Es gab einen [Antrag](https://gitlab.com/f3n/info/-/blob/master/mv2024/Antrag_Transparenzbericht.pdf) auf Ergänzung der Tagesordnung: Diskussion und Beschlussfassung über zukünftig zu erstellende Transparenzberichte.
Dieses Thema werden wir als Tagesordnungpunkt 11.1 unter dem Punkt 11 "Verschiedenes" behandeln.



Euer F3N-Vorstand
