J A H R E S B E R I C H T  2 0 2 1
----------------------------------

Jahresbericht des Vorstands gemäß §14 (11.4) der Satzung für das
Geschäftsjahr 2021:

Der Vorstand des Vereins hat im abgelaufenen Geschäftsjahr 11
Vorstandssitzungen abgehalten. Aufgrund der Pandemie wurden diese auch 2021
ausschließlich virtuell durchgeführt. Der im Jahr 2020 eingerichtete
Jistsi-Server stand auch im Jahr 2021 zur Verfügung - weiterhin ebenfalls für
die Öffentlichkeit.  
Die Beschlussfähigkeit der Vorstandssitzungen war jeweils gegeben. Mitglieder
des Vorstands waren regelmäßig bei den virtuellen Community-Treffen und
Stammtischen des Freifunk Aachen anwesend, um den Kontakt zur Community
sicherzustellen.

Zur Erreichung seiner satzungsgemäßen Ziele hat der Verein im vergangenen Jahr
folgende Tätigkeiten ausgeführt oder unterstützt:

- Mitgliederversammlung 2021 (pandemiebedingt virtuell abgehalten)
- Eintragung als gemeinnütziger Verein
- regelmäßige Rückmeldungen zur Förderung an die Stadt Aachen über 
  Rechenschaftsberichte
- Finanzierung von Internet-Bandbreite und Server-Housing für Freifunk Aachen
- Endabrechnung Förderantrag zur Anschaffung eigener Server
- Projekt "Richtfunk Welthaus Aachen" wurde pausiert

Als weitere Gremien gibt es die folgenden Teams:
- Team Düren
- Team Server/Backbone
- Team Herzogenrath
- Team Freifunk Welthaus

Die Teams stellen ihre Tätigkeiten bei der Mitgliederversammlung separat vor.

Der Vorstand dankt den Mitgliedern dieser Teams für ihre Arbeit.

Der Mitgliederstand des Vereins hat sich im vergangenen Jahr wie folgt
verändert:
- ordentliche Mitglieder
  - Mitgliederstand Anfang 2021 : 28
  - Mitgliedeintritte 2021      :  3
  - Mitgliedsaustritte 2021     :  1
  - Mitgliederstand 31.12.2021  : 30

Für das laufende Jahr hat der Vorstand folgende Schwerpunkte gesetzt:

1. weitere Sponsoren finden


Die finanzielle Situation des Vereins ist geordnet. Das Barvermögen beläuft
sich auf 13.522,68€. Dazu kommen offene Forderungen von Mitgliedsbeiträgen in
Höhe von 2.016€, sowie offene Verbindlichkeiten in Höhe von 505,75€. Hiervon
unterliegen 4.364,90€ einer Zweckbindung.

Der Kassenwart wird die einzelnen Positionen im Kassenbericht erläutern.

Zusammenfassend ist festzustellen, dass der Verein seine Ziele weiter verfolgt
hat und bezüglich der Rechenschaftsberichte und Förderung von Wissenweitergabe
Erfolge verbuchen konnte.

Besonders hervorheben möchten wir die weitere Unterstützung durch die Stadt
Aachen. Aber auch denjenigen gilt der Dank des Vereins, die durch ihre
uneigennützigen Schenkungen uns ermöglicht haben, unsere satzungsmäßigen
Aufgaben zu erfüllen.

Hier sind zu nennen:
- Stadt Stolberg
- einzelne Mitglieder, die freiwillig einen erhöhten Mitgliedsbeitrag gezahlt
  haben

Weiterhin freuen wir uns über die vielen Ehrenamtlichen und Unternehmen, die
Freifunk direkt unterstützen und somit mit uns gemeinsame Ziele fördern.
