Informationen zur Mitgliederversammlung am 20.04.2023, 19 Uhr
============================================================
(Stand 19.04.2023)

Liebe Mitglieder,

die Versammlung findet hier statt: [RelAix Networks GmbH, Auf der Hüls 172, 52068 Aachen](https://www.openstreetmap.org/search?query=Auf%20der%20H%C3%BCls%20172%2C%2052068%20Aachen#map=19/50.78353/6.13472)  
Getränke bitte wenn möglich selber mitbringen.

Die Präsentation kann hier eingesehen werden: [Präsentation](https://docs.google.com/presentation/d/1eLzs65G9ZKsZQ_f4K_rM5vD-yjSk4NJyJ41ugrqne70/edit?usp=sharing)

Das Protokoll zur Mitgliederversammlung entsteht während der Versammlung hier: [MV23 Protokoll](https://pad.freifunk.net/p/F3N-MV2023)

Der Jahresbericht kann hier eingesehen werden: [Jahresbericht](https://gitlab.com/f3n/info/-/blob/master/mv2023/jahresbericht2022.txt)

Den Kassenbericht findet Ihr hier: [Kassenbericht](https://gitlab.com/f3n/info/-/blob/master/mv2023/Kassen_und_Pruefbericht_2022.txt)

Euer F3N-Vorstand
