Informationen zur Mitgliederversammlung am 19.3.2020
====================================================
(Stand 18.3.2020)

Liebe Mitglieder,

wegen der Corona-Pandemie müssen wir euch nun mitteilen, dass uns
der reservierte Raum für die MV in der Digital Church nicht mehr zur
Verfügung steht.

Aus diesem Grund hat der Vorstand nun beschlossen keine
Mitgliederversammlung, auch nicht im kleinen Kreis, durchzuführen.

Sobald sich die Situation etwas beruhigt hat, Versammlungen wieder erlaubt
sind und Konferenzräume zur Verfügung stehen, wird der Vorstand die
Mitgliederversammlung schnellstmöglich nachholen und nochmals alle
Mitglieder einladen.

Bleibt weiterhin alle gesund.

Euer F3N-Vorstand

---

(Stand 16.3.2020)

Liebe Mitglieder,

wegen der Corona-Pandemie müssen wir auch bezüglich der
Mitgliederversammlung am 19.3.2020 mit Einschränkungen leben, um unseren
Beitrag zur Verlangsamung der Ausbreitung zu leisten.

Daher haben wir die außergewöhnliche Bitte, möglichst nicht
persönlich zur Mitgliederversammlung zu erscheinen.

Trotzdem möchten wir Euch umfassend informieren, und auch jedem die
Möglichkeit geben, sich zu äußern. Daher werden wir im Vorfeld
der Mitgliederversammlung am Dienstag ab 19:30 Uhr, also dann,
wenn normalerweise der Beratungstisch stattfindet, eine
Videokonferenz anbieten.

**Update 16.3.2020:** Für die Videokonferenz haben wir einen
[eigenen Server](https://meet.f3n-ac.de/freifunk) basierend auf
freier Software eingerichtet, damit Ihr ohne irgendeine Anmeldung
teilnehmen könnt.

Diskussionen können natürlich auch in [Slack](https://ffac.slack.com)
geführt werden. Wer noch keinen Account hat, kann sich über den
[Einladungslink](https://join.slack.com/t/ffac/shared_invite/zt-crrrf638-dg_CFadMP4NWSTnUs7QXkQ)
einen erstellen.

Wir hoffen, alle offenen Fragen soweit im Vorfeld klären zu können,
dass sich am Donnerstag nur 2 oder 3 Leute an der Church treffen
müssen, um formal die Mitgliederversammlung abzuhalten. Damit wird
das Ansteckungsrisiko auf ein Minimum reduziert. Bei halbwegs passendem
Wetter kann das ganze sogar vor der Tür an der frischen Luft stattfinden.

Neben den langweiligen Formalia (Wahl von Protokollführer und Sitzungsleiter)
gibt es eigentlich nur zwei Entscheidungen zu treffen:

- Entlastung des Vorstands
- Neuwahl des Vorstands

Der bisherige Vorstand steht vollständig wieder zur Wahl. Andere
Interessenten haben sich bisher nicht gemeldet. Eure Zustimmung
vorausgesetzt, wäre die Wiederwahl also auch schnell erledigt. Die
wenigen anwesenden Mitglieder würden die Wahl dann unter
Berücksichtigung von Vollmachten und Stimmrechtsweisungen durchführen.

Bezüglich der Entlastung bitten wir Euch, den
[Jahresbericht](https://gitlab.com/f3n/info/-/blob/master/mv2020/jahresbericht2020.txt) sowie den
[Kassenbericht](https://gitlab.com/f3n/info/-/blob/master/mv2020/kassenpruefbericht.pdf)
aufmerksam zu lesen, und eventuelle Fragen schon im Vorfeld zu stellen.

Die Kasse wurde von Daniel Müllers geprüft, die beiden anderen
Kassenprüfer waren zum Prüfungstermin verhindert. Christoph Franzen
hat die Belege nachträglich eingesehen und den Bericht als „sachlich
und rechnerisch richtig“ abgesegnet.

Sollte es irgendwelche Zweifel geben, werden wir den Tagesordnungspunkt
auf die nächste Mitgliederversammlung vertagen.

Trotz aller Bemühungen ist es weiterhin denkbar, dass die
Mitgliederversammlung ganz ausfallen muss, zum Beispiel, falls bis dahin
behördliche Ausgangssperren verhängt werden. In dem Fall werden wir
Euch natürlich umgehend informieren.

Euer F3N-Vorstand
