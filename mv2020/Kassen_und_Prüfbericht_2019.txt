Kassenbericht 2019, Stand 31.12.2019
------------------------------------

Überträge vom Vorjahr:
- Girokonto						6717,77€

Einnahmen 2019:

- Einnahmen aus Mitgliedsbeiträgen			 744,00€
- Einnahmen durch Schenkungen:				1009,57€
- Einnahmen durch öffentliche Förderung:	       12000,00€

- Einnahmen Gesamt:				       13753,57€

- Ausgaben Büro (Notar, Steuerberater):			 317,87€
- Ausgaben für Infrastruktur (Mailserver, Domains):	 127,71€
- Ausgaben Freifunktag:					  70,00€
- Ausgaben 'Erstattung Veranstaltungsteilnahmen':	  15,00€
- Ausgaben Internet:					4884,00€
- Ausgaben Material:					 108,69€
- Ausgaben Mitgliedschaften:				 250,00€
- Ausgaben Team Düren (Veranstaltungsraum):		 180,00€
							========
- Ausgaben Gesamt:					5953,27€
- Ausgleich offener Verbindlichkeiten aus Vorjahr        798,12€
                                                        ========
- In 2019 erfolgte Ausgaben:				6751,39€

Neue Kontostände:

Girokonto					       13712,95€
Barkasse                                                   7,00€
                                                       =========
Damit ergibt sich als Gesamtvermögen (ist)	       13719,95€

- Zzgl.  offene Forderungen Mitgliedsbeiträge:		 816,00€
- Abzgl. offene Verbindlichkeiten:	             (-)   0,00€

- Gesamtvermögen (soll)				       14535,95€

Verwendung zweckgebundener Mittel
---------------------------------

Von den Einnahmen unterlagen 12000€, vom Vermögen zu Jahresbeginn
4571,81€ einer engeren Zweckbindung als schon durch
die Satzung vorgegeben (Ratsantrag 330/17, 22.03.2018):
"Diese Mittel sollen für die Förderung der Aktivitäten des Vereins,
insbesondere für Betrieb und Wartung notwendiger Infrastrukur beim
Einsatz von Freifunk sowie zur Kostendeckung der Unterstützung der
Stadt Aachen bei der Ausleuchtung öffentlicher Einrichtungen dienen."

Hiervon wurden bezahlt:

- Ausgaben für Infrastruktur (Mailserver, Domains):	 127,71€
- Ausgaben abzgl. Einnahmen Freifunktag:		  70,00€
- Ausgaben Internet:					4884,00€
							========
							5081,71€

Vom Vermögen noch der Zweckbindung unterliegend	       11490,10€
Freies Vermögen						3045,85€





Jan Niehusmann (Kassenwart)


Prüfbericht der Kassenprüfer
----------------------------

Der schriftliche Kassenbericht des Vereins wurden von den in der
Mitgliederversammlung gewählten Kassenprüfern geprüft. Dabei wurden
auch die weiteren Unterlagen und die vorgelegten Belege eingesehen.

Übereinstimmung besteht auch bei den vorgelegten Vereins-Bankauszügen
und dem Abgleich mit den vollständig vorliegenden Belegen.

Es wurden keine Beanstandungen festgestellt.



Oliver Huberty, Christoph Franzen, Daniel Müllers (Kassenprüfer)

