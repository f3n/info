Informationen zur Mitgliederversammlung am 2.6.2022, 20 Uhr
===========================================================
(Stand 2.6.2022)

Liebe Mitglieder,

zur Videokonferenz der Mitgliederversammlung geht es hier entlang: [Videkonferenz](https://meet.f3n-ac.de/F3N-MV-2022)

Die Präsentation kann hier eingesehen werden: [Präsentation](https://docs.google.com/presentation/d/1lnGapO4DIoioqOIPzAOZZmv83rq4nAB02MZY6hTdvCE/edit?usp=sharing)

Das Protokoll zur Mitgliederversammlung entsteht während der Versammlung hier: [MV22 Protokoll](https://pad.freifunk.net/p/F3N-MV2022)

Der Jahresbericht kann hier eingesehen werden: [Jahresbericht](https://gitlab.com/f3n/info/-/blob/master/mv2022/jahresbericht2021.txt)

Den Kassenbericht findet Ihr hier: [Kassenbericht](https://gitlab.com/f3n/info/-/blob/master/mv2022/Kassenbericht_2021.txt)

Die Kasse wurde von Christoph Franzen, Daniel Müllers und Felix
Preuschoff geprüft. Unterschriften werden wir nachreichen, da die
Prüfung wegen Corona vollständig online erfolgte.

Bitte stellt nach Möglichkeit offene Fragen bereits im Vorfeld, damit
nicht wärend der Versammlung die erfragten Informationen zusammensuchen
müssen.

Euer F3N-Vorstand

