Informationen zur Mitgliederversammlung am 28.5.2020
====================================================
(Stand 25.5.2020)

Liebe Mitglieder,

da wir bei den wöchentlichen Stammtischen / Community-Treffen gute
Erfahrung mit Jitsi Meet gemacht haben, möchten wir auch für die
Mitgliederversammlung auf diese Software setzen.

Der Link für die Mitgliederversammlung lautet <https://meet.f3n-ac.de/mv2020>.

Bitte nutzt hierfür als Browser Chrome bzw. einen darauf basierenden Browser,
Firefox verursacht leider Performanceprobleme.

Die Versammlung beginnt um 20 Uhr. Um bei technischen Problemen noch
helfen zu können, werden wir ab 19:30 Uhr in dem Konferenzraum zu
finden sein. Solltet Ihr keine Verbindung aufbauen können, meldet Euch bitte,
z.B. per Mail an mv2020@f3n-ac.de oder im Slack-Raum #verein-mv2020.

Zum Üben werden wir auch am 26.5.2020 ab 20 Uhr in diesem Raum sein.

Mit einer Online-Mitgliederversammlung haben wir bisher keine Erfahrungen,
eventuell müssen wir ein wenig improvisieren, insbesondere bei
Abstimmungen. Wir zählen auf Euer Verständnis und etwas Geduld.

Zum Glück stehen keine komplizierten Entscheidungen an, voraussichtlich
ist die einzige Abstimmung, die wir irgendwie auszählen müssen,
die Vorstandswahl. Der bisherige Vorstand steht vollständig wieder
zur Wahl, andere Interessenten haben sich bisher nicht gemeldet.

Wir hoffen auf rege Teilnahme. Gerne können wir im Anschluss an den
offiziellen Teil gemeinsam, wenn auch nur per Videokonferenz, ein Bierchen
zusammen zu trinken.

Bezüglich der Entlastung bitten wir Euch, den
[Jahresbericht](https://gitlab.com/f3n/info/-/blob/master/mv2020/jahresbericht2020.txt) sowie den
[Kassenbericht](https://gitlab.com/f3n/info/-/blob/master/mv2020/kassenpruefbericht.pdf)
aufmerksam zu lesen, und eventuelle Fragen schon im Vorfeld zu stellen.

Die Kasse wurde von Daniel Müllers geprüft, die beiden anderen
Kassenprüfer waren zum Prüfungstermin verhindert. Christoph Franzen
und Oliver Huberty haben die Belege nachträglich eingesehen und den
Bericht als „sachlich und rechnerisch richtig“ abgesegnet.

Euer F3N-Vorstand
